# Markup Filter API #

An API for markup language filtering in Drupal.

This module provides an API to let other module developers define a set of valid tags and attributes (which may be valid within the context of 1 or more tags — including a case where an attribute may be valid within all tags). 

When enabled as an input filter, this module will poll all modules conforming to it's API for the set of elements and attributes which those modules define as valid, combine them into two big lists (one for attributes and one for elements), then filter out any element not in the element list and any attribute either not in the attribute list, or out of context as defined by the attribute list. 

This module will allow other modules conforming to it's API to add on to it's settings form if the module wants. It will also define it's own settings.



## Dependencies ##

* PHP 5
* [Drupal](http://drupal.org/) 6.x
  * Drupal core's `filter` module

## Installation ##

### Drupal 6 ###

1. Download into your site's `modules` folder (by default at `/sites/default/modules` — you may need to create the `modules` folder if it doesn't already exist).
2. Log in to your site as the super-administrator (user-id 1).
3. Enable the module from `Administer` -> `Site building` -> `Modules` (it will be in the `Input Filter` group). Also, enable the modules which conform to it's API and you want to use as a filter (try the `XHTML Modularization 1.1 Markup API plugin` module which comes packaged with `markup_api`).
4. Edit an input filter from `Administer` -> `Site configuration` -> `Input formats`, clicking `configure` in the `operations` column of the table listing the available input formats in the same row as the format you want to configure..
5. Enable the filter by checking the box labeled `Markup Filter API`, then clicking `Save configuration` at the bottom of the page.
6. Set your desired configuration from the `Configure` pane for the input format.
7. Ensure that the module is reasonably placed in the `Rearrange` pane for the input format. This module works best near the bottom, but above the HTML Corrector filter (if it's enabled for this format).

## Authors ##

 * M Parker — you can contact me via [my drupal.org account page](http://drupal.org/user/536298/contact)

## Future ##

This module will likely stay in beta until automated testing with [SimpleTest](http://drupal.org/project/simpletest) has been implemented. If you'd like to contribute tests, please do so — see the 'Contributing' section below for more information.

A feature that I'd really like to implement sometime is an "overrides" box which lets the administrator allow or deny elements and attributes. These allow/deny rules would override anything set by other modules that conform to the `markup_api` API.

I haven't really thought about how version numbering is going to work. I think it's tradition to increment the major version number if the API changes... but I'd also like to keep security updates + bug fixes on a separate track from the addition of new features. Obviously, since the project is still in Beta, this isn't a pressing concern right now, but feel free to [leave me a message](http://drupal.org/user/536298/contact) with your insight if you wish!

On the subject of new versions, I haven't had time to work on a Drupal 7 version yet, but I intend to do so at some time in the future.

## Contributing ##

Contributions are welcome! If you have new features, bug fixes or any code that you'd like to submit, [clone this project](http://drupal.org/node/1080784/git-instructions), make your changes, check that they conform to [the Drupal coding standards](http://drupal.org/coding-standards) (I suggest using the [Coder module](http://drupal.org/project/coder) to check), then [create a patch](http://drupal.org/node/1054616#creating-patches) and put it on [the issue queue](http://drupal.org/project/issues/1080784) to let me know about your changes.

Please note that, by submitting a patch, you licence your code to be used under the same terms as the rest of this project, i.e.: the GNU General Public License, version 2 or later (please also refer to the ["Yes, I promise to upload GPLv2+ code" issue at Drupal.org](http://drupal.org/node/720670)).

