<?php
/**
 * @file
 *   Defines functions needed by both the module and the install script
 */

/**
 * Makes a safe variable name for storing preferences in
 * @param $id
 *   A string containing the ID we want to store
 * @param $format
 *   The ID of the input format being used (to allow settings to be saved per-input-filter)
 * @return
 *   A string that should be unique enough to store preferences with
 */
function _markup_api_make_safe_varname($name, $format = 0) {
  if ($format != 0) {
    return 'markup_api_' . (string)$name . '_' . (string)intval($format);
  }
  else {
    return 'markup_api_' . (string)$name;
  }
}

/**
 * Makes a safe cache ID for storing data in
 * @param $id
 *   A string containing the ID we want to store
 * @param $format
 *   The ID of the input format being used (to allow settings to be saved per-input-filter)
 * @return
 *   A string that should be unique enough to serve as a cache ID
 */
function _markup_api_make_safe_cacheid($id, $format = 0) {
  if ($format != 0) {
    return 'markup_api_' . (string)intval($format) . '_' . (string)$id;
  }
  else {
    return 'markup_api_' . (string)$id;
  }
}

