<?php
/**
 * @file
 *   A object to filter things.
 *   Because our filter needs so much context, an object is useful because it automatically retains it's own state
 */

/** Ignore invalid elements/attributes */
define('MARKUP_API_INVALID_ACTION_IGNORE', 0);
/** Strip out invalid elements/attributes */
define('MARKUP_API_INVALID_ACTION_STRIP', 1);
/** Escape invalid elements/attributes */
define('MARKUP_API_INVALID_ACTION_ESCAPE', 2);

/** Don't do any case checking or changing */
define('MARKUP_API_CASE_SENSITIVE', 0);
/** Convert everything to lower case, then compare */
define('MARKUP_API_CASE_PREFER_LOWER', 1);
/** Convert everything to upper case, then compare */
define('MARKUP_API_CASE_PREFER_UPPER', 2);

/**
 * An object to do code filtering for us
 */
class MarkupFilterer {
  /**
   * An array of allowed elements
   * The element name should be the key of the array (array values are ignored)
   */
  private $elements_allowed;
  /** What to do with disallowed elements
   * This must be one of the defined constants beginning with MARKUP_API_INVALID_ACTION_
   */
  private $element_action;

  /**
   * An array of allowed attributes
   * The attribute name should be the key of the array
   */
  private $attributes_allowed;
  /**
   * What to do with disallowed attributes
   * This must be one of the defined constants beginning with MARKUP_API_INVALID_ACTION_
   */
  private $attribute_action;

  /**
   * What to do with comments
   * This must be one of the defined constants beginning with MARKUP_API_INVALID_ACTION_
   */
  private $comment_action;

  /**
   * An array of valid protocols
   * The protocol identifier is the key of the array (array values are ignored)
   */
  private $protocols_allowed;

  /**
   * A pointer to a function that we can call when comparing and converting
   */
  private $case_function;

  /**
   * A pointer to the function that array_walk-type functions can call when converting and comparing
   */
  private $inplace_case_function;

  /**
   * A helper function for $this->case_function. Does nothing to text passed in.
   * @param $value
   *   The text to do nothing with
   * @return
   *   Returns $value (our parameter) (i.e.: with nothing done to it)
   */
  private static function _case_do_nothing($value) {
    return $value;
  }

  /**
   * A helper function for $this->inplace_case_function. Does nothing to text passed in.
   * @param &$value
   *   The text to do nothing with
   */
  private static function _inplace_case_do_nothing(&$value) {
    // Noop!
  }

  /**
   * A helper function for $this->inplace_case_function. Converts the text passed in to lower-case
   * @param &$value
   *   The text to convert to lower case
   */
  private static function _inplace_case_tolower(&$value) {
    $value = drupal_strtolower($value);
  }

  /**
   * A helper function for $this->inplace_case_function. Converts the text passed in to upper case.
   * @param &$value
   *   The text to convert to upper case
   */
  private static function _inplace_case_toupper(&$value) {
    $value = drupal_strtoupper($value);
  }

  /**
   * Construct a filterer object
   * @param $elements_allowed
   *   An array of elements to allow. When filtering, anything not in this list will be filtered out according to $element_action
   * @param $attributes_allowed
   *   An array of attributes to allow. Each attribute name is a key in the array; while each element that this attribute is permitted in is a value in an array which is the value of the key of the name of the attribute. If you understood that, more power too you. Any attribute not in the list or in the wrong element is filtered.
   * @param $element_action
   *   The action to take if we find an element that's not allowed.
   * @param $attribute_action
   *   The action to take if we find an attribute that's not allowed.
   * @param $comment_action
   *   The action to take if we find a comment
   * @param $protocols_allowed
   *   An array of permitted URL protocols
   * @param $case_action
   *   How to compare attributes and elements... upper-case, lower-case or no-case-change
   */
  public function __construct($elements_allowed = array(), $attributes_allowed = array(), $element_action = MARKUP_API_INVALID_ACTION_STRIP, $attribute_action = MARKUP_API_INVALID_ACTION_STRIP, $comment_action = MARKUP_API_INVALID_ACTION_IGNORE, $protocols_allowed = array(), $case_action) {
    // Ensure that we compare things with the right function
    switch ($case_action) {
      case MARKUP_API_CASE_SENSITIVE:
        $this->case_function = array(&$this, '_case_do_nothing');
        $this->inplace_case_function = array(&$this, '_inplace_case_do_nothing');
        break;

      case MARKUP_API_CASE_PREFER_LOWER:
        $this->case_function = 'drupal_strtolower';
        $this->inplace_case_function = array(&$this, '_inplace_case_tolower');
        break;

      case MARKUP_API_CASE_PREFER_UPPER:
        $this->case_function = 'drupal_strtoupper';
        $this->inplace_case_function = array(&$this, '_inplace_case_toupper');
        break;
    }

    // Store arrays in the correct case
    array_walk($elements_allowed, $this->inplace_case_function);
    $this->elements_allowed = array_flip($elements_allowed);
    $this->attributes_allowed = $attributes_allowed;

    // Convert the list of protocols to an array
    foreach ($protocols_allowed as &$protocol) {
      $protocol = trim(drupal_strtolower($protocol));
    }
    $this->protocols_allowed = array_flip($protocols_allowed);

    // Store what we're supposed to do when we find something invalid
    $this->element_action = $element_action;
    $this->attribute_action = $attribute_action;
    $this->comment_action = $comment_action;
  }

  /**
   * Filter document text, according to the preferences we stored when we created the filterer
   * @param $text
   *   The text to filter.
   * @return
   *   Filtered text.
   */
  public function filter_elements($text) {
    return preg_replace_callback(
      '%' .
        '(?:' .                  // BEGIN (1)
            '<(?=[^a-zA-Z!/])' . // A lone < (not 2 because it's a positive lookahead)
          '|' .                // OR
            '<!--.*?-->' .       // An HTML comment
          '|' .                // OR
            '<[^>]*(?:>|$)' .    // A tag and everything inside (end character is a non-capturing subgroup)
          '|' .                // OR
            '>' .                // A lone >
        ')'.                   // END (1)
      '%x',                    // Ignore whitespace
      array(&$this, 'filter_element'),
      $text
    );
  }

  /**
   * Helper functon to filter out a disallowed element and any attributes it has
   *
   * Called back from preg_replace_callback() in filter_elements
   *
   * Based upon _filter_xss_split in Drupal core
   *
   * @see filter_elements()
   * @see _filter_xss_split()
   * @ingroup callbacks
   */
  private function filter_element($matched_element) {
    // preg_replace_callback returns an array, but I only care about the first one
    $matched_element = $matched_element[0];

    // Replace lone less-than/greater-than characters with their HTML entity replacement
    if (drupal_substr($matched_element, 0, 1) != '<') {
      return '&gt;';
    }
    elseif (drupal_strlen($matched_element) == 1) {
      return '&lt;';
    }

    // Split up the element into $parts. If nothing matches, we've got a seriously-malformed tag, so eat it. Nom nom nom.
    if (!preg_match(
      '%' .
        '^' .                // Start of string
        '(?:' .              // Start of non-capturing subgroup
            '<' .              // Tag opening angle-bracket
            '\s*' .            // Any whitespace after opening angle bracket
            '(/\s*)?' .        // Any non-whitespace; possibly the closing tag marker / (1)
            '([a-zA-Z0-9]+)' . // The tag name (2)
            '([^>]*)' .        // The tag's attributes(3)
            '>?' .             // Optional tag closing angle-bracket
          '|' .              // OR
            '(<!--.*?-->)' .   // A comment (4)
        ')' .   // Close non-capturing subgroup
        '$' .   // End of string
      '%',
      $matched_element, $parts
    )) {
      return '';
    }

    // Give meaning to the chaos
    $slash = trim($parts[1]);
    $element = &$parts[2];
    $attributes = &$parts[3];
    $comment = &$parts[4];

    // Special case: comments match differently than other elements; but they are still technically elements
    if ($comment) {
      $element = '!--';
    }

    // Keep comments if requested. Nom nom nom!
    if ($comment) {
      switch ($this->comment_action) {
        case MARKUP_API_INVALID_ACTION_IGNORE:
          return $comment;
        case MARKUP_API_INVALID_ACTION_STRIP:
          return '';
        case MARKUP_API_INVALID_ACTION_ESCAPE:
          return check_plain($comment);
      }
    }

    // If the element is not in our allowed list, eat it! Nom nom nom!
    if (!isset($this->elements_allowed[call_user_func($this->case_function, $element)])) {
      switch ($this->element_action) {
        case MARKUP_API_INVALID_ACTION_IGNORE: // Why would you want to do this? It defeats the purpose!
          return $matched_element;
        case MARKUP_API_INVALID_ACTION_STRIP:
          return '';
        case MARKUP_API_INVALID_ACTION_ESCAPE:
          return check_plain($matched_element);
      }
    }

    // If this was an end–element, make sure we output a valid one
    if ($slash != '') {
      return '</' . $element . '>';
    }

    // Check if there's a closing slash at the end of the attributes
    if (preg_match(
      '%' .
        '\s?' . // Optional whitespace at beginning
        '/' .   // The slash we're checking for
        '\s*' . // More optional whitespace at end
        '$' .   // End of string
      '%',
      $attributes
    )) {
      $xhtml_slash = '/';
    }
    else {
      $xhtml_slash = '';
    }

    // Remove the closing slash from the list of attributes
    $attributes = preg_replace(
      '%' .
        '(\s?)' . // Optional whitespace at beginning
        '/' .     // The slash we're checking for
        '\s*' .   // More optional whitespace at the end
        '$' .     // End of string
      '%',
      '\1', $attributes
    );

    // Eat any bad attributes
    $clean_attributes = implode(' ', $this->filter_attributes($attributes, $element));

    // Eat any angle brackets in the attributes
    $clean_attributes = preg_replace('/[<>]/', '', $clean_attributes);

    // If we happened to eat all attributes, then ensure we don't output anything; otherwise add a space to the beginning of the attributes
    $clean_attributes = drupal_strlen($clean_attributes) ? ' ' . $clean_attributes : '';

    // Return a valid XML start– or empty–element
    return '<' . $element . $clean_attributes . $xhtml_slash  . '>';
  }

  /**
   * Filter inside-element text, according to the settings stored when the object was created
   *
   * Based upon _filter_xss_attributes in Drupal core
   *
   * @param $all_attributes
   *   A string containing all attributes for us to filter
   * @param $element
   *   An optional string containing the element being parsed
   * @return
   *   Filtered text
   * @see _filter_xss_attributes()
   */
  protected function filter_attributes($all_attributes, $element = '') {
    // An array of valid attributes to return
    $answer = array();

    // Helps us figure out what we are supposed to parse
    //   0 = Attribute name, 1 = Equals sign or valueless attribute, 2 = Attribute value
    $mode = 0;

    // Easy way to keep track of the name of the attribute we are parsing
    $attribute_name = '';

    // Loop through each attribute from left to right
    while (drupal_strlen($all_attributes) != 0) {
      // "Successful" flag
      $working = 0;

      switch ($mode) {
        // If we're supposed to parse the attribute name...
        case 0:
          // If we successfully capture a valid attribute name
          if (preg_match('/^([-a-zA-Z]+)/', $all_attributes, $match)) {
            // Set the attribute name
            $attribute_name = call_user_func($this->case_function, $match[1]);

            // Eat any invalid attributes. Nom nom nom.
            $keep = $this->_attribute_allowed($attribute_name, $element);

            // Everything's valid so far; also, let's parse the = next
            $working = $mode = 1;
            // Eat the attribute name
            $all_attributes = preg_replace('/^[-a-zA-Z]+/', '', $all_attributes);
          }
          break;

        // If we're supposed to parse the equals sign or valueless attribute ("selected")...
        case 1:
          // Match the equals sign and any whitespace around it
          if (preg_match('/^\s*=\s*/', $all_attributes)) {
            // Everything's valid so far...
            $working = 1;
            // Let's parse the value next
            $mode = 2;
            // Eat the equals sign and any whitespace around it.
            $all_attributes = preg_replace('/^\s*=\s*/', '', $all_attributes);
            break;
          }
          // If we matched nothing in the previous match, this must be a valueless attribute. Try to match the whitespace.
          if (preg_match('/^\s+/', $all_attributes)) {
            // Everything's valid so far...
            $working = 1;
            // Let's parse a new attribute next
            $mode = 0;
            // If we DIDN'T just finish eating an invalid attribute, add the (valueless) attribute to the list of validated attributes
            if ($keep) {
              $answer[] = $attribute_name;
            }
            // Now eat the whitespace
            $all_attributes = preg_replace('/^\s+/', '', $all_attributes);
          }
          break;

        // If we're supposed to parse the attribute value...
        case 2:
          // Try to match the attrbute value (assuming it's in double-quotes)
          if (preg_match('/^"([^"]*)"(\s+|$)/', $all_attributes, $match)) {
            // Eat the value if it's in a disallowed protocol
            $thisval = $this->filter_attribute_value($match[1]);
            // If we DIDN'T just finish eating an invalid attribute, add the attribute-name/attribute-value pair to the list of validated attributes (with double-quotes because the writer presumably used double-quotes for a good reason)
            if ($keep) {
              $answer[] = $attribute_name . '="' . $thisval . '"';
            }
            // Everything's valid so far...
            $working = 1;
            // Let's parse a new attribute next
            $mode = 0;
            // Now eat the value (assuming it's in double-quotes)
            $all_attributes = preg_replace('/^"[^"]*"(\s+|$)/', '', $all_attributes);
            break;
          }

          // Try to match the attrbute value (assuming it's in single-quotes)
          if (preg_match("/^'([^']*)'(\s+|$)/", $all_attributes, $match)) {
            // Eat the value if it's in a disallowed protocol
            $thisval = $this->filter_attribute_value($match[1]);
            // If we DIDN'T just finish eating an invalid attribute, add the attribute-name/attribute-value pair to the list of validated attributes (with single-quotes because the writer presumably used single-quotes for a good reason)
            if ($keep) {
              $answer[] = $attribute_name . '=\'' . $thisval . '\'';
            }
            // Everything's valid so far...
            $working = 1;
            // Let's parse a new attribute next
            $mode = 0;
            // Now eat the value (assuming it's in single-quotes)
            $all_attributes = preg_replace("/^'[^']*'(\s+|$)/", '', $all_attributes);
            break;
          }

          // Try to match the attribute name (assuming it's without quotes)
          if (preg_match("%^([^\s\"']+)(\s+|$)%", $all_attributes, $match)) {
            // Eat the value if it's in a disallowed protocol
            $thisval = $this->filter_attribute_value($match[1]);
            // If we DIDN'T just finish eating an invalid attribute, add the attribute-name/attribute-value pair to the list of validated attributes (with double-quotes because it seems the writer cannot write valid XML)
            if ($keep) {
              $answer[] = $answer . '="' . $thisval . '"';
            }
            // Everything's valid so far; Let's parse a new attribute next
            $working = 1; $mode = 0;
            // Now eat the value (assuming it's without quotes)
            $all_attributes = preg_replace("%^[^\s\"']+(\s+|$)%", '', $all_attributes);
          }
          break;
      }

      // If the last one wasn't well formed, eat it and try again from the top
      if ($working == 0) {
        $all_attributes = preg_replace(
          '/' .
            '^' .                   // Start of the string
            '(' .                   // BEGIN (1)
                '"[^"]*("|$)' .       // A string that starts with a double quote, up until the next double quote or the end of the string (2 is the closing " or EOS)
              '|' .                 // OR
                '\'[^\']*(\'|$)|' .   // A string that starts with a quote, up until the next quote or the end of the string (3 is the closing ' or EOS)
              '|' .                 // OR
                '\S' .                // A non-whitespace character
            ')*' .                  // Any number of the above three; END (1)
            '\s*' .                 // Any number of whitespaces
          '/x'                      // Ignore whitespace
          , '', $all_attributes
        );
        // Go to the next iteration of the loop assuming that the next attribute will be sane
        $mode = 0;
      }
    }

    // Special case: if the attribute list ends with a valueless attribute, it won't match above; so just add it here
    if ($mode == 1) {
      $answer[] = $attribute_name;
    }
    return $answer;
  }

  /**
   * Filter attribute value text.
   * If the attribute value is a URI, and the protocol it uses wasn't whitelisted when the object was created, then get rid of it.
   * @param $attribute_value
   *   The value we are filtering
   * @return
   *   A filtered value
   */
  protected function filter_attribute_value($attribute_value) {
    // Iteratively remove any invalid protocols
    do {
      $before = $attribute_value;
      $colon_position = strpos($attribute_value, ':');
      // If we find a colon, it could be a protocol name
      if ($colon_position > 0) {
        // Verify
        $protocol = drupal_substr($attribute_value, 0, $colon_position);
        // If a colon is preceeded by a slash, question mark or hash, it cannot possibly be part of the URL scheme. This must be a relative URL, which inherits the (safe) protocol of the base document.
        if (preg_match('%[/?#]%', $protocol)) {
          break;
        }
        // As per RFC 2616, section 3.2.3 (URI Comparison), scheme comparison must be case-insensitive.
        // If this is a disallowed protocol, eat it. Nom nom nom.
        if (!$this->protocols_allowed[strtolower($protocol)]) {
          $attribute_value = drupal_substr($attribute_value, $colon_position + 1);
        }
      }
    } while ($before != $attribute_value);
    return check_plain($attribute_value);
  }

  /**
   * Test whether an attribute is allowed.
   * @param $attribute_name
   *   The name of the attribute that could or could not be allowed.
   * @param $element_name
   *   The name of the element that the user is attempting to use $attribute_name in.
   * @return
   *   TRUE if the attribute is allowed; FALSE otherwise
   */
  private function _attribute_allowed($attribute_name, $element_name) {
    // Fix the parameters before comparing them
    $attribute_name = call_user_func($this->case_function, $attribute_name);
    $element_name = call_user_func($this->case_function, $element_name);

    // Go for further testing if the attribute is in the list somewhere
    if (isset($this->attributes_allowed[$attribute_name])) {
      // If no contextual elements are supplied, then assume the attribute is valid in all elements
      if (count($this->attributes_allowed[$attribute_name]) < 1) {
        return TRUE;
      }
      // If we are in the right context, allow the element
      elseif (in_array($element_name, $this->attributes_allowed[$attribute_name])) {
        return TRUE;
      }
    }

    // Otherwise, return false
    return FALSE;
  }
} // class MarkupFilterer

