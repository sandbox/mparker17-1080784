<?php
/**
 * @file
 *   Defines functions needed by both the module and the install script
 */

/**
 * Makes a safe variable name for storing preferences in
 * @param $id
 *   A string containing the ID we want to store
 * @param $format
 *   The ID of the input format being used (to allow settings to be saved per-input-filter)
 * @return
 *   A string that should be unique enough to store preferences with
 */
function _markup_xhtml_modularization_make_safe_var_name($name, $format = 0) {
  if ($format != 0) {
    return 'markup_xhtml_modularization_' . (string)$name . '_' . (string)intval($format);
  }
  else {
    return 'markup_xhtml_modularization_' . (string)$name;
  }
}

