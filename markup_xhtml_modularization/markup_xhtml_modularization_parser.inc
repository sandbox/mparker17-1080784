<?php
/**
 * @file
 * Defines a number of functions to be used when parsing a configuration array.
 */

/**
 * Gets the unique identifier of the current structured array
 * @param $array
 *   The array to find the identifier-name of
 * @return
 *   The unique-name of the array, or null if the array doesn't have a name
 */
function _markup_xhtml_modularization_get_id(&$array) {
  return $array['#id'];
}

/**
 * Gets the (translated) common name of the current structured array
 * @param $array
 *   The array to find the common name
 * @return
 *   The translated common name
 */
function _markup_xhtml_modularization_get_name(&$array) {
  return $array['#name'];
}

/**
 * Gets the type of the current structured array
 * @param $array
 *   The array to find the type
 * @return
 *   The type
 */
function _markup_xhtml_modularization_get_type(&$array) {
  return $array['#type'];
}

/**
 * Gets the expected data type for the current structured array
 * @param $array
 *   The array to find the expected data type in
 * @return
 *   The expected data type
 */
function _markup_xhtml_modularization_get_validate(&$array) {
  return $array['#validate'];
}

/**
 * Gets import directives for the current structured array
 * @param $array
 *   The array to find the import directives in
 * @return
 *   An array of directives to import
 */
function _markup_xhtml_modularization_get_import(&$array) {
  return $array['#import'];
}

/**
 * Copy-by-reference an item from one array to another
 * @param &$value
 *   The original value to shallow copy from
 * @param &$dest_array
 *   The destination array
 * @param $key = ''
 *   The key to copy. If it's not given, or the key exists, then the value is added to the end of the array.
 */
function _markup_xhtml_modularization_shallowcopy_item(&$value, &$dest_array, $key = '') {
  if (!empty($key) and !array_key_exists($key, &$dest_array)) {
    $dest_array[$key] =& $value;
  }
  else {
    $dest_array[] =& $value;
  }
}

/**
 * Copy-by-reference a key/value pair into another array based on a test function; throwing an error if the key already exists
 * @param &$value
 *   The value to test and copy into the new array if the test returns true
 * @param $key
 *   The key of the value being tested. Not used; but must be included if this function is to be called by array_walk and array_walk_recursive
 * @param &$dest_array
 *   The array to add the key/value pair to
 * @param $callback_name
 *   The name of a function to use as a test. If the function returns true, then the specified key/value pair will by copied into the array.
 *   The only parameter passed to this function is $value
 */
function _markup_xhtml_modularization_shallowcopy_on_test(&$value, $key, &$dest_array, $callback_name) {
  if (!is_callable($callback_name)) {
    throw new Exception('The function passed is not callable!');
  }

  if (call_user_func($callback_name, &$value)) {
    _markup_xhtml_modularization_shallowcopy_item(&$value, &$dest_array);
  }
}

/**
 * PHP's 'array_walk_recursive' function doesn't act on key/value pairs if the value is another array: it happily goes right into that array without stopping.
 * This isn't what I want... I need it to stop on __every__ key/value pair; including ones where the value is an array.
 * @see http://php.net/manual/en/function.array-walk-recursive.php
 * @param $array
 *   The input array.
 * @param $funcname
 *   Typically, funcname takes on two parameters. The array parameter's value being the first, and the key/index second.
 *   Users may not change the array itself from the callback function. e.g. Add/delete elements, unset elements, etc. If the array that array_walk() is applied to is changed, the behavior of this function is undefined, and unpredictable.
 * @param $userdata
 *   If the optional userdata parameter is supplied, it will be passed as the third parameter to the callback funcname.
 */
function _markup_xhtml_modularization_my_arraywalk_recursive(&$array, $funcname, &$userdata) {
  if (!is_callable($funcname)) {
    throw new Exception('The function passed is not callable!');
  }

  foreach ($array as $key => &$value) {
    // Always perform the base case
    $parameters = array(&$value, $key);
    if (is_array($userdata)) {
      $parameters[] =& $userdata;
    }
    call_user_func_array($funcname, &$parameters);

    // If the value is an array, perform the recursive case
    if (is_array($value)) {
      _markup_xhtml_modularization_my_arraywalk_recursive($value, $funcname, $userdata);
    }
  }
}

/**
 * So, it turns out that PHP's array_walk_recursive can only take a maximum of three arguments. Sadly, I need more than that. This function gets around this limit.
 * @param &$value
 *   The value to test and copy to the new array if the test returns true
 * @param $key
 *   The key of the value being tested. Not used; but must be included if this function is to be called by array_walk and array_walk_recursive
 * @param &$otherparams
 *   An array containing the other parameters to be passed to _markup_xhtml_modularization_awr_compat_shallowcopy_on_test
 */
function _markup_xhtml_modularization_awr_compat_shallowcopy_on_test(&$value, $key, &$otherparams) {
  _markup_xhtml_modularization_shallowcopy_on_test(&$value, $key, &$otherparams[0], $otherparams[1]);
}


/**
 * Returns true if the array passed to it is an XHTML module group.
 * @see markup_xhtml_modularization_defn.inc
 * @param $array
 *   The array to test
 * @return
 *   True if the array is an XHTML module group
 */
function _markup_xhtml_modularization_testis_modulegroup(&$array) {
  return is_array(&$array) and array_key_exists('#type', &$array) and $array['#type'] == 'module_group';
}

/**
 * Returns true if the array passed to it is an XHTML module group and isn't marked as "non-defining"
 * @see markup_xhtml_modularization_defn.inc
 * @param $array
 *   The array to test
 * @return
 *   True if the array is a defining XHTML module group
 */
function _markup_xhtml_modularization_testis_defining_modulegroup(&$array) {
  return is_array(&$array) and array_key_exists('#type', &$array) and $array['#type'] == 'module_group' and !array_key_exists('#non_defining', &$array);
}

/**
 * Returns true if the array passed to it is a XHTML module
 * @see markup_xhtml_modularization_defn.inc
 * @param $array
 *   The array to test
 * @return
 *   True if the array is an XHTML module
 */
function _markup_xhtml_modularization_testis_module(&$array) {
  return is_array(&$array) and array_key_exists('#type', &$array) and $array['#type'] == 'module';
}

/**
 * Returns true if the array passed to it is a XHTML module and isn't marked as "non-defining"
 * @see markup_xhtml_modularization_defn.inc
 * @param $array
 *   The array to test
 * @return
 *   True if the array is a defining XHTML module
 */
function _markup_xhtml_modularization_testis_defining_module(&$array) {
  return is_array(&$array) and array_key_exists('#type', &$array) and $array['#type'] == 'module' and !array_key_exists('#non_defining', &$array);
}

/**
 * Returns true if the array passed to it is an XHTML element
 * @see markup_xhtml_modularization_defn.inc
 * @param $array
 *   The array to test
 * @return
 *   True if the array is an XHTML element
 */
function _markup_xhtml_modularization_testis_element(&$array) {
  return is_array(&$array) and array_key_exists('#type', &$array) and $array['#type'] == 'element';
}

/**
 * Returns true if the array passed to it is an XHTML element and isn't marked as "non-defining"
 * @see markup_xhtml_modularization_defn.inc
 * @param $array
 *   The array to test
 * @return
 *   True if the array is a defining XHTML element
 */
function _markup_xhtml_modularization_testis_defining_element(&$array) {
  return is_array(&$array) and array_key_exists('#type', &$array) and $array['#type'] == 'element' and !array_key_exists('#non_defining', &$array);
}

/**
 * Returns true if the array passed to it is an attribute
 * @see markup_xhtml_modularization_defn.inc
 * @param $array
 *   The array to test
 * @return
 *   True if the array is an XHTML attribute
 */
function _markup_xhtml_modularization_testis_attribute(&$array) {
  return is_array(&$array) and array_key_exists('#type', &$array) and $array['#type'] == 'attribute';
}

/**
 * Returns true if the array passed to it is an attribute and isn't marked as "non-defining"
 * @see markup_xhtml_modularization_defn.inc
 * @param $array
 *   The array to test
 * @return
 *   True if the array is a defining XHTML attribute
 */
function _markup_xhtml_modularization_testis_defining_attribute(&$array) {
  return is_array(&$array) and array_key_exists('#type', &$array) and $array['#type'] == 'attribute' and !array_key_exists('#non_defining', &$array);
}

/**
 * Returns true if the array passed to it is an attribute collection
 * @see markup_xhtml_modularization_defn.inc
 * @param $array
 *   The array to test
 * @return
 *   True if the array is an XHTML attribute collection
 */
function _markup_xhtml_modularization_testis_attribute_collection(&$array) {
  return is_array(&$array) and array_key_exists('#type', &$array) and $array['#type'] == 'attribute_collection';
}

/**
 * Returns true if the array passed to it is an attribute collection and isn't marked as "non-defining"
 * @see markup_xhtml_modularization_defn.inc
 * @param $array
 *   The array to test
 * @return
 *   True if the array is a defining XHTML attribute collection
 */
function _markup_xhtml_modularization_testis_defining_attribute_collection(&$array) {
  return is_array(&$array) and array_key_exists('#type', &$array) and $array['#type'] == 'attribute_collection' and !array_key_exists('#non_defining', &$array);
}

