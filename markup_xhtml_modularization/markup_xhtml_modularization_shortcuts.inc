<?php
/**
 * @file
 * Defines a number of parser shortcuts.
 */

/**
 * Recursively get all module groups in the current array
 * @param &$array
 *   The array to look through
 * @return
 *   An array of all the module groups
 */
function _markup_xhtml_modularization_rget_module_groups(&$array, $is_defining = FALSE) {
  $answer = array();
  $params = array(&$answer, $is_defining ? '_markup_xhtml_modularization_testis_defining_modulegroup' : '_markup_xhtml_modularization_testis_modulegroup');
  _markup_xhtml_modularization_my_arraywalk_recursive(&$array, '_markup_xhtml_modularization_awr_compat_shallowcopy_on_test', &$params);
  return $answer;
}

/**
 * Recursively get all modules in the current array
 * @param &$array
 *   The array to look through
 * @return
 *   An array of all the modules
 */
function _markup_xhtml_modularization_rget_modules(&$array, $is_defining = FALSE) {
  $answer = array();
  $params = array(&$answer, $is_defining ? '_markup_xhtml_modularization_testis_defining_module' : '_markup_xhtml_modularization_testis_module');
  _markup_xhtml_modularization_my_arraywalk_recursive(&$array, '_markup_xhtml_modularization_awr_compat_shallowcopy_on_test', &$params);
  return $answer;
}

/**
 * Recursively get all elements in the current array
 * @param &$array
 *   The array to look through
 * @return
 *   An array of all the elements
 */
function _markup_xhtml_modularization_rget_elements(&$array, $is_defining = FALSE) {
  $answer = array();
  $params = array(&$answer, $is_defining ? '_markup_xhtml_modularization_testis_defining_element' : '_markup_xhtml_modularization_testis_element');
  _markup_xhtml_modularization_my_arraywalk_recursive(&$array, '_markup_xhtml_modularization_awr_compat_shallowcopy_on_test', &$params);
  return $answer;
}

/**
 * Recursively get all attributes in the current array
 * @param &$array
 *   The array to look through
 * @return
 *   An array of all the attributes
 */
function _markup_xhtml_modularization_rget_attributes(&$array, $is_defining = FALSE) {
  $answer = array();
  $params = array(&$answer, $is_defining ? '_markup_xhtml_modularization_testis_defining_attribute' : '_markup_xhtml_modularization_testis_attribute');
  _markup_xhtml_modularization_my_arraywalk_recursive(&$array, '_markup_xhtml_modularization_awr_compat_shallowcopy_on_test', &$params);
  return $answer;
}

/**
 * Recursively get all attribute collections in the current array
 * @param &$array
 *   The array to look through
 * @return
 *   An array of all the attributes
 */
function _markup_xhtml_modularization_rget_attribute_collections(&$array, $is_defining = FALSE) {
  $answer = array();
  $params = array(&$answer, $is_defining ? '_markup_xhtml_modularization_testis_defining_attribute_collection' : '_markup_xhtml_modularization_testis_attribute_collection');
  _markup_xhtml_modularization_my_arraywalk_recursive(&$array, '_markup_xhtml_modularization_awr_compat_shallowcopy_on_test', &$params);
  return $answer;
}

/**
 * Get all module groups immediately in the current array
 * @param &$array
 *   The array to look through
 * @return
 *   An array of all the module groups
 */
function _markup_xhtml_modularization_get_module_groups(&$array, $is_defining = FALSE) {
  $answer = array();
  $params = array(&$answer, $is_defining ? '_markup_xhtml_modularization_testis_defining_modulegroup' : '_markup_xhtml_modularization_testis_modulegroup');
  array_walk(&$array, '_markup_xhtml_modularization_awr_compat_shallowcopy_on_test', &$params);
  return $answer;
}

/**
 * Get all modules immediately in the current array
 * @param &$array
 *   The array to look through
 * @return
 *   An array of all the modules
 */
function _markup_xhtml_modularization_get_modules(&$array, $is_defining = FALSE) {
  $answer = array();
  $params = array(&$answer, $is_defining ? '_markup_xhtml_modularization_testis_defining_module' : '_markup_xhtml_modularization_testis_module');
  array_walk(&$array, '_markup_xhtml_modularization_awr_compat_shallowcopy_on_test', &$params);
  return $answer;
}

/**
 * Get all elements immediately in the current array
 * @param &$array
 *   The array to look through
 * @return
 *   An array of all the elements
 */
function _markup_xhtml_modularization_get_elements(&$array, $is_defining = FALSE) {
  $answer = array();
  $params = array(&$answer, $is_defining ? '_markup_xhtml_modularization_testis_defining_element' : '_markup_xhtml_modularization_testis_element');
  array_walk(&$array, '_markup_xhtml_modularization_awr_compat_shallowcopy_on_test', &$params);
  return $answer;
}

/**
 * Get all attributes immediately in the current array
 * @param &$array
 *   The array to look through
 * @return
 *   An array of all the attributes
 */
function _markup_xhtml_modularization_get_attributes(&$array, $is_defining = FALSE) {
  $answer = array();
  $params = array(&$answer, $is_defining ? '_markup_xhtml_modularization_testis_defining_attribute' : '_markup_xhtml_modularization_testis_attribute');
  array_walk(&$array, '_markup_xhtml_modularization_awr_compat_shallowcopy_on_test', &$params);
  return $answer;
}

/**
 * Get all attribute collections immediately in the current array
 * @param &$array
 *   The array to look through
 * @return
 *   An array of all the attribute collections
 */
function _markup_xhtml_modularization_get_attribute_collections(&$array, $is_defining = FALSE) {
  $answer = array();
  $params = array(&$answer, $is_defining ? '_markup_xhtml_modularization_testis_defining_attribute_collection' : '_markup_xhtml_modularization_testis_attribute_collection');
  array_walk(&$array, '_markup_xhtml_modularization_awr_compat_shallowcopy_on_test', &$params);
  return $answer;
}

/**
 * Lists the IDs found in an array by a certain callback, prefixed and/or suffixed by your choice of strings.
 * @param &$array
 *   The array to go through
 * @param $callback
 *   The function to use to go through the array
 * @param $beforeach
 *   A string to prefix each ID with
 * @param $aftereach
 *   A string to suffix each ID with
 * @return
 *   An array of IDs, prefixed and/or suffixed by a string
 */
function _markup_xhtml_modularization_list_ids(&$array, $callback, $beforeeach = '', $aftereach = '', $defining = FALSE) {
  $answer = array();
  foreach ($callback(&$array, $defining) as $fragment) {
    $answer[] = $beforeeach . (string)_markup_xhtml_modularization_get_id($fragment) . $aftereach;
  }
  return $answer;
}

